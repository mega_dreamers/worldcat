$(function () {

});

function sendToCart() {
    var data = $("#send_to_cart").serialize();
    $.ajax({
        type: 'POST',
        url: '/products/processing_form',
        data: data,
        success: function() {
            alert('Товар добавлен в корзину');
            $.post('/cart_items', function(data){
                $('#cart_total').html(data)
            })
        }
    });
    return false;
}