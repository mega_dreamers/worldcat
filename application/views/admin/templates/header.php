<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Paweł 'kilab' Balicki - kilab.pl" />
    <title><?php echo $page_title, ' - ', $page_name ?></title>
    <link rel="stylesheet" type="text/css" href="/css/adm_style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/css/navi.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/css/development.css" media="screen" />
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
        });
    </script>
    <style>
        .messages {
            display: inline-block;
            margin-left: 34%;
            border: 0px solid;
            border-radius: 5px;
            background-color: #e1e1e1;
            color: #000000;
            text-align: center;
            width: 26px;
            height: 18px;
        }
        #mess {
            display: inline-block;
            margin-top: 3px;
        }
    </style>
</head>
<body>
<div class="wrap" style=" font-size: 14px; font-weight: bold">
    <div id="header">
        <div id="top">
            <div class="left">
                <p style="font-size: 20px"><strong>W</strong>orld<strong>C</strong>at</p>
            </div>
            <div class="right">
                <div class="align-right">
                    <p>Welcome, <strong><?php echo $username;?></strong> [ <a href="/admin/logout">logout</a> ]</p>
                </div>
            </div>
        </div>
        <?php if(empty($username)) {echo 'not found';}?>
        <div id="nav">
            <ul>
                <li class="upp"><a href="#">Main control</a>
                    <ul>
                        <li>&#8250; <a href="/admin/home">Home</a></li>
                        <li>&#8250; <a href="/admin/pages">Show all pages</a></li>
                        <li>&#8250; <a href="">Add new page</a></li>
                        <li>&#8250; <a href="">Site config</a></li>
                    </ul>
                </li>
                <li class="upp"><a href="#">Manage content</a>
                    <ul>
                        <li>&#8250; <a href="">Reports</a></li>
                        <li>&#8250; <a href="">Add new page</a></li>
                        <li>&#8250; <a href="">Add new gallery</a></li>
                        <li>&#8250; <a href="">Categories</a></li>
                    </ul>
                </li>
                <li class="upp"><a href="#">Users</a>
                    <ul>
                        <li>&#8250; <a href="">Show all uses</a></li>
                        <li>&#8250; <a href="">Add new user</a></li>
                        <li>&#8250; <a href="">Lock users</a></li>
                    </ul>
                </li>
                <li class="upp"><a href="#">Settings</a>
                    <ul>
                        <li>&#8250; <a href="">Site configuration</a></li>
                        <li>&#8250; <a href="">Contact Form</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div id="content">
        <div id="sidebar">
            <div class="box">
                <div class="h_title">&#8250; Main control</div>
                <ul id="home">
                    <li class="b1"><a class="icon view_page" href="/admin/home">Home</a></li>
                    <li class="b2"><a class="icon page" href="/admin/pages">Show all pages</a></li>
                    <li class="b1"><a class="icon page" href="/admin/goods">Show all goods</a></li>
                    <li class="b2"><a class="icon category" href="/admin/categories">Categories</a></li>
                    <!--<li class="b1"><a class="icon add_page" href="/admin/new_page">Add new page</a></li>-->
                    <li class="b1"><a class="icon contact" href="/admin/messages">Messages</a><?php if($count_mess != 0) {echo '<font class="messages"><b id="mess">+'.$count_mess.'</b></font>';} ?></li>
                    <li class="b2"><a class="icon report" href="/admin/orders">Orders</a></li>
                    <!--<li class="b1"><a class="icon photo" href="/admin/images">Show images for slide</a></li>-->
                    <li class="b2"><a class="icon config" href="/admin/config">Site config</a></li>
                </ul>
            </div>
            <?php if($session_id == 1 AND $username === 'admin'): ?>
                <div class="box">
                    <div class="h_title">&#8250; Users</div>
                    <ul>
                        <li class="b1"><a class="icon users" href="/admin/users">Show all users</a></li>
                        <li class="b2"><a class="icon add_user" href="/admin/create_user">Add new user</a></li>
                        <!--<li class="b1"><a class="icon block_users" href="">Lock users</a></li>-->
                    </ul>
                </div>
            <?php endif; ?>
            <!--<div class="box">
                <div class="h_title">&#8250; Manage content</div>
                <ul>
                    <li class="b2"><a class="icon report" href="">Reports</a></li>
                    <li class="b2"><a class="icon add_page" href="">Add new page</a></li>
                </ul>
            </div>-->
        </div>