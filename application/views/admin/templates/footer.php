<div id="footer">
    <div class="left">
        <p>Developer: <a href="https://plus.google.com/u/0/103399909738350117110/">Makarenko Nikita</a> | Admin Panel: <a href="http://worldcat.pe.hu/">worldcat.pe.hu</a></p>
    </div>
    <div class="right">
        <script>
            $('a').each(function() {
                var a = new RegExp('/' + window.location.host + '/');
                if(!a.test(this.href)) {
                    $(this).click(function(event) {
                        event.preventDefault();
                        event.stopPropagation();
                        window.open(this.href, '_blank');
                    });
                }
            });
        </script>
        <p><a href="/" target="_blank">Go to site</a></p>
    </div>
</div>
</div>

</body>
</html>
