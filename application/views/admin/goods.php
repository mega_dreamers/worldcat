<script type="text/javascript">
    $(function(){
        $(".product #delete").click(function(){
            $(".product").animate({ opacity: 'hide' }, "slow");
        });
    });
</script>
<div id="main">
    <div class="full_w">
        <div class="h_title">Manage goods - table</div>

        <?php if(!empty($goods)): ?>
        <table>
            <thead>
            <tr>
                <th scope="col" style="width: 45px;"><a href="/admin/goods" style="color: #DACDCD">ID</a></th>
                <th scope="col"><a href="/admin/goods/1" style="color: #DACDCD">Title</a></th>
                <th scope="col" style="width: 65px;"><a href="/admin/goods/2" style="color: #DACDCD">Gategory ID</a></th>
                <th scope="col" style="width: 75px;"><a href="/admin/goods/3" style="color: #DACDCD">Gategory</a></th>
                <th scope="col" style="width: 55px;"><a href="/admin/goods/4" style="color: #DACDCD">Price</a></th>
                <th scope="col">Image</th>
                <th scope="col" style="width: 55px;">Modify</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach($goods as $key => $j): ?>
                <tr class="product">
                    <td class="align-center"><?php echo $j->goods_id;?></td>
                    <td><?php echo $j->goods_title;?></td>
                    <td class="align-center"><?php echo $j->categ_id;?></td>
                    <td class="align-center"><?php echo $j->title;?></td>
                    <td class="align-center">$<?php echo $j->price;?></td>
                    <td class="align-center" width="120px"><img src="/upload/timthumb.php?src=goods/<?php echo $j->image; ?>&w=80&h=70" style="border-radius: 5px"/></td>
                    <td>
                        <a href="/admin/goods_edit/<?php echo $j->goods_id;?>" class="table-icon edit" title="Edit"></a>
                        <!--<a href="#" class="table-icon archive" title="Archive"></a>-->
                        <a href="/admin/goods_delete/<?php echo $j->goods_id;?>" class="table-icon delete" id="delete" title="Delete"></a>
                    </td>
                </tr>
            <?php endforeach ?>
            <?php else: echo "<div class='n_warning'><p><b>no categories</b></p></div>";?>
            <?php endif;?>
            </tr>
            </tbody>
        </table>
        <div class="entry">
            <div class="sep"></div>
            <a class="button add" href="/admin/new_goods">Add new goods</a>
        </div>
    </div>
</div>