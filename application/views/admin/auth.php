<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Paweł 'kilab' Balicki - kilab.pl" />
<title>SimpleAdmin</title>
<link rel="stylesheet" type="text/css" href="/css/login.css" media="screen" />
</head>
<body>
<div class="wrap">
	<div id="content">
		<div id="main">
			<div class="full_w">
				<form action="" method="post">
					<?php if(isset($error_message)): ?>
						<div class="n_error"><p><?php echo $error_message; ?></p></div>
					<?php elseif(!empty(validation_errors())): ?>
						<div class="n_error"><?php echo validation_errors(); ?></div>
					<?php endif; ?>
					<label for="login">Username:</label>
					<input id="login" name="username" type="text" class="text" />
					<label for="pass">Password:</label>
					<input id="pass" name="password" type="password" class="text" />
					<?php if(!empty($captcha)):?>
						<label><?= $captcha;?></label>
						<input type="text" name="captcha" class="text" />
					<?php endif; ?>
					<div class="sep"></div>
					<button type="submit" class="ok">Login</button>
				</form>
			</div>
			<div class="footer">&raquo; <a href="/">WorldCat</a> | Admin Panel</div>
		</div>
	</div>
</div>

</body>
</html>
