<div id="main">
<div class="full_w">
    <div class="h_title">Manage pages - table</div>

    <!--<div class="entry">
        <div class="sep"></div>
    </div>-->
    <table>
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Link</th>
            <th scope="col" style="width: 65px;">Modify</th>
        </tr>
        </thead>

        <tbody>
        <?php if(!empty($get_admin_menu)): ?>
            <?php foreach($get_admin_menu as $key => $j): ?>
                <tr>
                    <td class="align-center"><?php echo $j->id;?></td>
                    <td><?php echo $j->title;?></td>
                    <td><?php echo $j->link;?></td>
                    <td>
                        <a href="/admin/page_edit/<?php echo $j->id; ?>" class="table-icon edit" title="Edit"></a>
                        <!--<a href="#" class="table-icon archive" title="Archive"></a>-->
                        <a href="/admin/page_delete/<?php echo $j->id; ?>" class="table-icon delete" title="Delete"></a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else: echo "error";?>
        <?php endif;?>
        </tr>
        </tbody>
    </table>
    <div class="entry">
        <!--<div class="pagination">
            <span>« First</span>
            <span class="active">1</span>
            <a href="">2</a>
            <a href="">3</a>
            <a href="">4</a>
            <span>...</span>
            <a href="">23</a>
            <a href="">24</a>
            <a href="">Last »</a>
        </div>-->
        <div class="sep"></div>
        <a class="button add" href="/admin/new_page">Add new page</a> <a class="button" href="/admin/categories">Categories</a>
    </div>
</div>
</div>