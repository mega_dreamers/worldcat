<div id="main">
    <div class="full_w">
        <div class="h_title">Edit page - form elements</div>
        <?php if(!empty(validation_errors())): ?>
            <div class="n_error"><?php echo validation_errors(); ?></div>
            <div class="sep"></div>
        <?php endif; ?>
        <form action="" method="post">
            <div class="element">
                <label for="title">Page title <span>(required)</span></label>
                <input id="title" name="title" class="text" value="<?php echo $content->title;?>"/>
            </div>
            <div class="element">
                <label for="name">Page link <span class="red">(required)</span></label>
                <input id="link" name="link" class="text err" value="<?php echo $content->link;?>"/>
            </div>
            <div class="element">
                <label for="name">Description</label>
                <input id="link" name="description" class="text" value="<?php echo $content->meta_desc;?>"/>
            </div>
            <div class="element">
                <label for="name">Keys</label>
                <input id="link" name="keys" class="text" value="<?php echo $content->meta_key;?>"/>
            </div>
            <div class="element">
                <label for="content">Page content <span>(required)</span></label>
                <textarea name="text" class="textarea" rows="10"><?php echo $content->text;?></textarea>
            </div>
            <div class="entry">
                <button type="submit" class="ok">Save page</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
            </div>
        </form>
    </div>
</div>