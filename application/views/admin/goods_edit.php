<div id="main">
    <div class="full_w">
        <div class="goods_edit">
            <div class="h_title">Add new goods - form elements</div>
            <?php if(!empty(validation_errors())): ?>
                <div class="n_error"><?php echo validation_errors(); ?></div>
            <?php elseif(!empty($warning)): ?>
                <div class="n_error"><?php echo $warning; ?></div>
            <?php endif; ?>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="element">
                    <label for="title">Title <span>(required)</span></label>
                    <input id="title" name="title" class="text" value="<?php echo $content->goods_title ?>" />
                </div>
                <div class="element">
                    <label for="category">Category <span class="red">(required)</span></label>
                    <select name="category" class="err">
                        <option value="<?php echo $content->categ_id ?>">Default</option>
                        <?php foreach($categories as $c): ?>
                            <option value="<?php echo $c->categ_id ?>"><?php echo $c->title ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="element">
                    <label for="content">Price $<input id="title" name="price" class="text" style="width: 30px; height: 8px" value="<?= $content->price ?>"/></label>
                </div>
                <div class="element">
                    <label for="best"><input type="checkbox" name="best" value="1" id="best" <?php if(!empty($checked)) {echo $checked;} ?> /> View on home page in slide</label>
                </div>
                <div class="element">
                    <label for="name">Description</label>
                    <input id="link" name="description" class="text" value="<?php echo $content->meta_desc;?>"/>
                </div>
                <div class="element">
                    <label for="name">Keys</label>
                    <input id="link" name="keys" class="text" value="<?php echo $content->meta_key;?>"/>
                </div>
                <div class="element">
                    <label for="content">Text <span>(required)</span></label>
                    <textarea name="text" class="textarea" rows="10"><?php echo $content->text ?></textarea>
                </div>
                <div class="element">
                    <label for="attach">Image</label>
                    <img src="/upload/timthumb.php?src=/goods/<?php echo $content->image ?>&w=80&h=70"style="border-radius: 5px"/><br /><br />
                    <input type="file" name="userfile">
                </div>
                <div class="element">
                    <button type="submit" class="ok">Save page</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
                </div>
                <?php if(!empty($get_comment)):?>
                    <div class="element">
                        <label for="attach">Comments:</label>
                        <?php foreach($get_comment as $j): ?>
                            <div id="comment">
                                <label for="name_and_data" style="font-style: italic"><?= strip_tags($j->name); ?>, <?= $j->date; ?>: <a href="/admin/comment/delete/<?= $j->product_id.'/'.$j->id ?>" id="delete">Delete</a></label>
                                <label for="comment" style="font-size: 15px"><?= strip_tags($j->comment); ?></label>
                            </div>
                        <?php endforeach;?>
                    </div>
                <?php endif; ?>
            </form>
        </div>
    </div>
</div>