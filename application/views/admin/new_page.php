<div id="main">
    <div class="full_w">
    <div class="h_title">Add new page - form elements</div>
    <?php if(!empty(validation_errors())): ?>
        <div class="n_error"><?php echo validation_errors(); ?></div>
        <div class="sep"></div>
    <?php endif; ?>
    <form action="" method="post">
        <div class="element">
            <label for="title">Page title <span>(required)</span></label>
            <input id="title" name="title" class="text" value="<?= set_value('title'); ?>" />
        </div>
        <div class="element">
            <label for="name">Page link <span class="red">(required)</span></label>
            http://worldcat.pe.hu/page/ <input id="link" name="link" class="text err" style="width: 483px" />
        </div>
        <div class="element">
            <label for="name">Description</label>
            <input id="link" name="description" class="text" />
        </div>
        <div class="element">
            <label for="name">Keys</label>
            <input id="link" name="keys" class="text" />
        </div>
        <!--<div class="element">
            <label for="attach">Attachments</label>
            <input type="file" name="attach" />
        </div>-->
        <div class="element">
            <label for="content">Page content <span>(required)</span></label>
            <textarea name="text" class="textarea" rows="10"><?= set_value('text'); ?></textarea>
        </div>
        <div class="entry">
            <button type="submit" class="ok">Save page</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
        </div>
    </form>
    </div>
</div>