<link rel="stylesheet" href="/css/style.css" xmlns="http://www.w3.org/1999/html">
<pre><script>
        $(function(){
            $("#slides").slides({
                width: 720,
                height: 300
            });
        });
    </script></pre>

<div id="main">
    <?php if ($content->link == 'home'): ?>
        <div id='container' style="margin-top:10px">
            <div id='slides'>
                <?php if(!empty($get_image)): ?>
                    <?php foreach($get_image AS $j): ?>
                        <div style="position: relative; z-index: 1;">
                            <img src="/upload/timthumb.php?src=/goods/<?= $j->image; ?>&w=720&h=300" alt="Slide <?= $n = 1; ?>" />
                            <div id="slide_bg">
                                <div id="slide_text">
                                    <a href="/product/<?= $j->goods_id; ?>" class="slide_link" style="color: orange;"><?= $j->goods_title; ?></a><br />
                                    Цена: <div style="color: green; display: inline; font-size: 19px">$<?= $j->price; ?></div><br />
                                    Описание: <?= substr($j->text, 0, 64); ?>...<a href="/product/<?= $j->goods_id; ?>" class="slide_link" style="color: orange; float: right; font-size: 13px">Подробнее...</a><br />
                                </div>
                            </div>
                        </div>
                        <?php $n++; ?>
                    <?php endforeach; ?>
                <?php else: echo 'not fount'; ?>
                <?php endif; ?>
            </div>

            <a href='#' class='controls'>Play</a>
            <p class='current_slide'></p>
        </div>
    <?php endif; ?>

    <div class="shell" style="margin-top:20px">
        <?= nl2br($content->text); ?>
    </div>
</div>
<!--<a href="/goods" class="link" style="color: orange;">товарах</a>-->