<div id="main">
    <div class="shell">
        <div class="cart">
            <table style="width: 100%">
                <?php if(!empty($this->cart->contents())): ?>
                    <tr style="border: 1px solid;">
                        <th class="qty"><i>id</i></th>
                        <th class="qty"><i>Фото</i></th>
                        <th class="name" style="text-align: center"><i>Наименование товара<i></th>
                        <th class="qty"><i>Цена</i></th>
                        <th class="qty"><i>Кол-во</i></th>
                        <th class="price"><i>Сумма</i></th>
                        <th class="other"><i>Действия</i></th>
                    </tr>

                    <?php foreach($this->cart->contents() as $item): ?>
                        <form method="post" name="cart" action="/cart/edit/<?=$item['rowid'];?>">
                            <tr style="border: 1px solid;">
                                <td class="qty"><i><?= $item['id']?></i></td>
                                <td class="qty"><i><a href="/product/<?= $data[$item['id']]->goods_id?>"><img src="/upload/timthumb.php?src=/goods/<?= $data[$item['id']]->image;?>&w=80&h=70" class="images"></a></i></td>
                                <td class="name"><b style="margin-left: 5px"><a href="/product/<?= $data[$item['id']]->goods_id?>"><?= $data[$item['id']]->goods_title?></a></b></td>
                                <td class="qty"><b>$<?= $item['price'];?></b></td>
                                <td class="qty"><input type="text" name="qty" class="qty_input" maxlength="2" value="<?= $item['qty'];?>" onchange="forms['cart'].submit()"></td>
                                <td class="price"><b>$<?= $item['price']*$item['qty'];?></b></td>
                                <td class="other"><b><a href="/cart/delete/<?=$item['rowid'];?>" id="delete">Удалить</a>
                            </tr>
                        </form>
                    <?php endforeach; ?>

                        <tr>
                            <td class="" id="end">
                            <td class="" id="end"></td>
                            <td class="name" id="end"><br /><b><a href="/cart/delete_all" style="color: #ff0000;"><u>Очистить корзину</u></a></td></td>
                            <td class="" id="end"></td>
                            <td class="qty" id="end"><br /><b>Итого</b></td>
                            <td class="price" id="end"><br /><b>$<?= $this->cart->total(); ?></b></td>
                            <td class="other" id="end" style="font-size: 11px"></b></td>
                        </tr>
                <?php elseif(empty($this->cart->contents())): ?>
                    <tr>
                        <td style="text-align: center; font-size: 20px; border: 2px dashed; color: #ff0000">Корзина пуста!</td>
                    </tr>
                <?php endif; ?>
            </table>

            <?php if(!empty($this->cart->contents())): ?>
                <br />
                <hr>
                <br />
                <form method="post" action="" style="text-align: right">
                    <font id="error"><?= form_error('name'); ?></font>
                    <b>Имя: <input type="text" name="name" value="<?= set_value('name');?>"><br /><br /></b>

                    <font id="error"><?= form_error('telephone'); ?></font>
                    <b>Телефон: <input type="text" name="telephone" value="<?= set_value('telephone');?>"><br /><br /></b>

                    <font id="error"><?= form_error('email'); ?></font>
                    <b>Email: <input type="email" name="email" value="<?= set_value('email');?>"><br /><br />

                    <button type="submit" id="order">Оформить заказ</button>
                </form>
            <?php endif; ?>
        </div>
    </div>
</div>