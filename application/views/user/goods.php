<div id="featured-content">
    <div class="shell">
        <p style="font-size: 14px; display: inline"><?php echo $about; foreach($categories as $j){echo "<a href='/goods/$j->categ_id'>$j->title</a>&nbsp;&nbsp;";}?></p>
        <form action="" method="get" style="display: inline; float: right;">
            <input name="search" type="text" placeholder=" Поиск..." style="border: 1px solid; border-radius: 100px; vertical-align: middle; height: 16px; width: 180px" />
            <input type="image" src="/img/poisk_strelka.png" style="width: 17px; margin-top: 1.4px; vertical-align: middle; left: -18px; position: relative;">
        </form>
    </div>
</div>

<div id="main" class="goods">
    <div class="shell" style="width: 1000px">
        <?php if(!empty($output['form_error'])): ?>
            <div style="font-size: 14px; text-align: center; margin-top: 3%"><?= $output['form_error']; ?></div>
        <?php elseif(!empty($list_goods) OR !empty($output['list_goods'])): ?>
            <?php $goods = (!empty($output['list_goods'])) ? $output['list_goods'] : $list_goods; ?>
            <?php foreach($goods as $j): ?>
                <div id="list">
                    <div id="image"><a href="/product/<?= $j->goods_id; ?>"><img src="/upload/timthumb.php?src=/goods/<?= $j->image;?>&w=170&h=180" style="border-radius: 7px;"></a></div>
                    <table>
                        <th id="title">
                            <b id="b">
                                <a href="/product/<?= $j->goods_id; ?>">&nbsp;<?= $j->goods_title; ?></a>
                            </b>
                        </th>
                        <th id="price">
                            <b id="b">$<?= $j->price; ?></b>
                        </th>
                    </table>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div style="font-size: 14px; text-align: center; margin-top: 3%">Товаров <?php if(!empty($output['request'])) {echo "по запросу \"{$output['request']}\"";}?> нет</div>
        <?php endif; ?>
        <?php if(!empty($pagination)): ?>
            <div style="font-size: 14px; text-align: center; margin-top: 4%"><?= $pagination; ?></div>
        <?php endif; ?>
    </div>
</div>