<div id="main">
    <div class="shell" style="width: 1000px;">
        <?php if(!empty($goods)): ?>
            <div class="product">
                <div id="img">
                    <a href="/upload/goods/<?= $goods->image;?>"><img src="/upload/timthumb.php?src=/goods/<?= $goods->image;?>&w=255&h=270" align="left" style="border-radius: 10px; margin-right: 15px"></a>
                </div>
                <div id="desc">
                    <div style="font-size: 20px; color: #271772; margin-left: 50%"><?= $goods->goods_title?></div>
                    <div style="font-size: 10px; color: #271772; margin-left: 50%">Категория: <a href="/goods/<?=$goods->categ_id;?>" style="color: orange" class="link"><?= $goods->title?></a></div><br />
                    <span style="font-size: 18px;"><?= nl2br($goods->text)?></span><br /><br />
                    <form method="post" action='javascript:void(null);' onsubmit='sendToCart()' id='send_to_cart'>
                        <input type="hidden" name="form_type" value="buy">
                        <input type="hidden" name="goods_id" value="<?= $goods->goods_id; ?>">
                        <b id="buy_font">
                            Цена: <b style="color: darkgreen; font-size: 17px">$<?= $goods->price; ?></b><br /><br />
                            Кол-во:<input type="text" value="1" name="qty" maxlength="2" style="text-align: center;"> шт.&nbsp;
                            <button type="submit">Купить</button>
                        </b>
                    </form>
                    <br />
                    <font id="error"><?= form_error('qty'); ?></font>
                    <a href="/goods"><?= '<br />','<<Назад'; ?></a>
                </div>
            </div>

            <div class="recommended">
                <b>Рекомендуемые товары:</b> <br />
                <?php if(!empty($recommended)): ?>
                    <?php foreach($recommended as $j): ?>
                        <?php if($goods->goods_id !== $j->goods_id): ?>
                            <div id="products">
                                <div id="image"><a href="/product/<?= $j->goods_id; ?>"><img src="/upload/timthumb.php?src=/goods/<?= $j->image;?>&w=170&h=180" style="border-radius: 7px;"></a></div>
                                <table><th id="title"><b><a href="/product/<?= $j->goods_id; ?>">&nbsp;<?= $j->goods_title; ?></a></b></th><th id="price"><b>$<?= $j->price; ?></b></th></table>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <div class="comments" style="border-bottom: 0px">
                <form method="post" action='' style="margin-top: 10px; margin-bottom: 10px">
                    <input type="hidden" name="form_type" value="comment">

                    <font id="error"><?= form_error('name'); ?></font>
                    Имя: <input type="text" name="name" maxlength="15" value="<?= set_value('name');?>"><br /><br />

                    Коментарий: <br />
                    <font id="error"><?= form_error('comment'); ?></font>
                    <textarea name="comment"><?= set_value('comment');?></textarea><br/><br />

                    <input type="submit" name="send" value="Отправить">
                </form>
                <?php if(!empty($get_comment)):?>
                <div class="comments">
                    <?php foreach($get_comment as $j): ?>
                        <div id="comment">
                            <b><?= strip_tags($j->name); ?></b>, <?= $j->date; ?>: <br />
                            <?= strip_tags($j->comment); ?>
                        </div>
                    <?php endforeach;?>
                </div>
                <?php endif; ?>
            </div>
        <?php endif;?>
    </div>
</div>