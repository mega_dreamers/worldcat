<div id="featured-content">
    <div class="shell">
        <p><?php echo $about; ?></p>
    </div>
</div>

<br />

<div class="shell">
    <div class="feedback">
        <form action="/feedback" method="post">
            <h4>Имя</h4>
            <font id="error"><?= form_error('name'); ?></font>
            <input type="text" name="name" id="text" value="<?php echo set_value('name');?>" />

            <h4>E-mail</h4>
            <font id="error"><?= form_error('email'); ?></font>
            <input type="email" name="email" id="text" value="<?php set_value('email');?>"/><br /><br />

            <font id="error"><?= form_error('message'); ?></font>
            <textarea name="message" id="textarea"></textarea><br /><br />
            <input type="submit" id="submit" value="Отправить"><br /><br />
            <?php if(isset($sussed)): ?>
                <div id="sussed"><?= $sussed;?></div><br />
                <a href="/">&lsaquo;&lsaquo;Домой</a>
            <?php endif;?>
        </form>
    </div>
</div>