<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_title, ' - ', $site_name ?></title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />

    <meta name="description" content="<?php if(!empty($meta_desc)){echo $meta_desc;}?>" />
    <meta name="keywords" content="<?php if(!empty($meta_key)){echo $meta_key;}?>" />

    <link rel="stylesheet" href="/css/style.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <script src="/js/slides.js"></script>

    <script>
        /*
         Get the curent slide
         */
        function currentSlide( current ) {
            $(".current_slide").text(current + " of " + $("#slides").slides("status","total") );
        }

        $(function(){
            $("#slides").slides({
                width: 720,
                height: 300
            });

            /*
             Play/stop button
             */
            $(".controls").click(function(e) {
                e.preventDefault();

                // Example status method usage
                var slidesStatus = $("#slides").slides("status","state");

                if (!slidesStatus || slidesStatus === "stopped") {

                    // Example play method usage
                    $("#slides").slides("play");

                    // Change text
                    $(this).text("Stop");
                } else {

                    // Example stop method usage
                    $("#slides").slides("stop");

                    // Change text
                    $(this).text("Play");
                }
            });
        });
    </script>


    <link rel="stylesheet" href="/css/development.css" type="text/css" />
    <link rel="stylesheet" href="/css/style_one.css" type="text/css" />
<link rel="shortcut icon" href="/css/images/favicon.ico" type="image/x-icon" />

</head>
<body>
<!-- START PAGE SOURCE -->
<div id="header">
    <div class="shell">
        <div id="logo-holder">
            <div id="logo" style="margin-left: 1cm; font-size: 13px; color: #ff4800"><font style="color: #000000;">W</font>orld <font style="color: #000000">C</font>at</div>
            <center id="logo"><a href="/"><img src="/img/timthumb.php?src=/logo/<?= $site_logo; ?>&w=130" align="middle"></a></center>
        </div>
        <div id="navigation">
            <ul>
                <?php if(!empty($get_menu)): ?>
                    <?php foreach($get_menu as $key => $j): ?>
                        <li><a href="/page/<?php echo $j->link;?>"><span><?php echo $j->title; ?></span></a></li>
                    <?php endforeach ?>
                    <li><a href="/goods"><span style="color: #ff0000">Товары</span></a></li>
                    <li><a href="/cart" style="color: #2348b6"><span id="cart_total"><img src="/img/cart.png" style="width: 35px"><?= $this->cart->total_items();?></span></a></li>
                <?php else: echo 'id not fount'; ?>
                <?php endif; ?>
            </ul>
        </div>
        <div id="logo-holder"><h1 id="logo"><?= $logo_name ?></h1></div>
        <a href="/promotions"><img src="/upload/timthumb.php?src=promotions.png&w=90" title="Промо-акция!" style="margin-left: 26px; margin-top: 15px"></a>
        <div class="cl">&nbsp;</div>

    </div>
</div>