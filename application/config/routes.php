<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "user/home/index/home";
$route['404_override'] = 'errors/error_404';

#User:
$route['page/(:any)'] = 'user/home/index/$1';

$route['promotions'] = 'user/home/promotions';
$route['test'] = 'user/home/test';

$route['feedback'] = 'user/feedback/index';

$route['goods'] = 'user/goods/index';
$route['goods/(:any)'] = 'user/goods/index/$1';
$route['goods/(:any)/(:any)'] = 'user/goods/index/$1/$2';

$route['product/(:any)'] = 'user/product/index/$1';
$route['products/(:any)'] = 'user/product/$1';

$route['cart'] = 'user/cart/index';
$route['cart/order_completed'] = 'user/cart/order_completed';
$route['cart/(:any)'] = 'user/cart/index/$1';

$route['cart_items'] = 'user/cart/show_total_items';

$route['search'] = 'user/goods/search';
$route['search/(:any)'] = 'user/goods/get_search/$1';

//$route['tth/(:any)'] = 'tth/$1';

#Admin:
$route['admin'] = 'admin/auth/index';
$route['admin/logout'] = 'admin/auth/logout';
$route['admin/home'] = 'admin/home/index';

$route['admin/pages'] = 'admin/pages/index';
$route['admin/new_page'] = 'admin/pages/new_page';
$route['admin/page_edit/(:any)'] = 'admin/pages/page_edit/$1';

$route['admin/categories'] = 'admin/categories/index';
$route['admin/categ_edit/(:any)'] = 'admin/categories/categories_edit/$1';

$route['admin/new_goods'] = 'admin/goods/add';
$route['admin/goods_edit/(:any)'] = 'admin/goods/edit/$1';
$route['admin/goods/(:any)'] = 'admin/goods/index/$1';

$route['admin/messages'] = 'admin/messages/index';
$route['admin/mess_read/(:any)'] = 'admin/messages/edit_status/$1';

$route['admin/orders'] = 'admin/orders/index';
$route['admin/order/(:any)'] = 'admin/orders/order/$1';

$route['admin/users'] = 'admin/accounts/index';
$route['admin/create_user'] = 'admin/accounts/create';
$route['admin/user_edit/(:any)'] = 'admin/accounts/edit/$1';

$route['admin/config'] = 'admin/site_config/index';

#Admin/delete:
$route['admin/page_delete/(:any)'] = 'admin/pages/delete/$1';
$route['admin/mess_delete/(:any)'] = 'admin/messages/delete/$1';
$route['admin/img_delete/(:any)'] = 'admin/images/delete/$1';
$route['admin/categ_delete/(:any)'] = 'admin/categories/delete/$1';
$route['admin/goods_delete/(:any)'] = 'admin/goods/delete/$1';
$route['admin/user_delete/(:any)'] = 'admin/accounts/delete/$1';
$route['admin/order_delete/(:any)'] = 'admin/orders/delete/$1';
$route['admin/comment/delete/(:any)/(:any)'] = 'admin/goods/delete_comment/$1/$2';

/* End of file routes.php */
/* Location: ./application/config/routes.php */