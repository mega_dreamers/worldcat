<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 12.02.15
 * Time: 15:35
 */

class Base_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_content($link = '') {
        return $this->db->get_where('pages', array('link' => $link))->row();
    }

    public function get_menu() {
        return $this->db->select('id, title, link')->get('pages')->result();
    }

    public function feedback($name, $email, $message) {
        $query = array (
            'name' => $name,
            'email' => $email,
            'message' => $message
        );

        $this->db->insert('feedback', $query);
    }

    public function slide() {
        return $this->db->get_where('goods', array('best' => 1))->result();
    }

    public function basket_name($link) {
        return $this->db->get_where('goods', array('goods_id' => $link))->row();
    }

    public function add_customer($data = array()) {
        $this->db->insert('customer', $data);
    }

    public function add_order($data = array()) {
        $this->db->insert('orders', $data);
    }

    public function id() {
        return $this->db->insert_id();
    }

    public function recording_comment($name, $comment, $product_id, $date) {
        $this->db->insert('comments', array('name' => $name, 'comment' => $comment, 'product_id' => $product_id, 'date' => $date));
    }

    public function recommended($id) {
        return $this->db->select('c.*, g.*')->join('categories c', 'c.categ_id=g.categ_id')->limit(5)->get_where('goods g', array('c.categ_id' => $id))->result();
    }
}