<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 09.07.15
 * Time: 16:18
 */

class A_Site_config_model extends CI_Model {
    public function get($item) {
        return $this->db->get_where('config', array('name' => $item))->row();
    }

    public function update($config) {
        $this->db->update('config', array('value' => $config['name']), array('name' => 'site_name'));
        $this->db->update('config', array('value' => $config['image']), array('name' => 'site_logo'));
    }
}
