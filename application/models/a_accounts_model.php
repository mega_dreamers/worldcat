<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 08.07.15
 * Time: 14:54
 */

class A_Accounts_model extends CI_Model {
    public function get() {
        return $this->db->get('admin')->result();
    }

    public function get_item($id) {
        return $this->db->get_where('admin', array('id' => $id))->row();
    }

    public function add($data) {
        $this->db->insert('admin', array('username' => $data['username'], 'password' => $data['password']));
    }

    public function edit($data){
        $this->db->update('admin', array('username' => $data['username'], 'password' => $data['password']), array('id' => $data['id']));
    }

    public function delete($id) {
        $this->db->delete('admin', array('id' => $id));
    }
}