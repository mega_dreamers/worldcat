<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 02.07.15
 * Time: 14:59
 */

class A_Messages_model extends CI_Model {
    public function messages() {
        return $this->db->get('feedback')->result();
    }

    public function edit($id) {
        $this->db->update('feedback', array('status' => 'read'), array('id_message' => $id));
    }

    public function delete($id) { // Удаление сообщеньки
        $this->db->delete('feedback', array('id_message' => $id));
    }
}