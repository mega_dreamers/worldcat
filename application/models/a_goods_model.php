<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 07.07.15
 * Time: 15:52
 */

class A_Goods_model extends CI_Model {
    public function goods($sort = 0) {
        switch($sort){
            case 1 : $column = 'goods_title';   break;
            case 2 : $column = 'c.categ_id';    break;
            case 3 : $column = 'title';         break;
            case 4 : $column = 'price';         break;
            default : $column = 'goods_id';     break;
        }

        return $this->db->select('c.*, g.*')->join('categories c', 'c.categ_id=g.categ_id')->order_by($column)->get('goods g')->result();
    }

    public function content($id){
        return $this->db->get_where('goods', array('goods_id' => $id))->row();
    }

    public function create($goods) {
        $this->db->insert('goods', $goods);
    }

    public function edit($id, $goods) {
        $this->db->update('goods', $goods, "goods_id = $id");
    }

    public function delete($id) {
        $file = $this->db->get_where('goods', array('goods_id' => $id))->row();
        $path = './upload/goods';
        unlink($path.DIRECTORY_SEPARATOR.$file->image);
        $this->db->delete('goods', array('goods_id' => $id));
        $this->db->delete('comments', array('product_id' => $id));
    }

    public function get_comment($id) {
        return $this->db->get_where('comments', array('product_id' => $id))->result();
    }

    public function delete_comment($id) {
        $this->db->delete('comments', array('id' => $id));
    }
}