<?php

/**
 * Class My_controller
 * @property base_model base_model
 * @property u_goods_model u_goods_model
 * @property a_site_config_model a_site_config_model
 * @property CI_Loader load
 * @property CI_session session
 * @property CI_Form_validation form_validation
 */

class My_controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Europe/Kiev');

        $this->load->model(array(
            'base_model',
            'u_goods_model',
            'a_site_config_model'
        ));
        $this->load->library(array('form_validation', 'cart'));

        $this->form_validation->set_message('required', 'Это поле не должно быть пустым');
        $this->form_validation->set_message('alpha_rus', '%s должно состоять только из букв');
        $this->form_validation->set_message('min_length', '%s слишком короткое');
        $this->form_validation->set_message('max_length', '%s слишком длинное');

        $site_name = $this->a_site_config_model->get('site_name');
        $site_logo = $this->a_site_config_model->get('site_logo');

        $this->settings = array(
            'site_name' => $site_name->value,
            'site_logo' => $site_logo->value,
            'site_title' => '',
            'email' => 'world@cat.pe.hu'
        );
    }

    protected function set_title($string) {
        $this->settings['site_title'] = $string;
    }

    protected function template($index, $data) {
        $data['logo_name'] = $this->settings['site_name'];
        $data['get_menu'] = $this->base_model->get_menu();
        $data['cart_total'] = $this->cart->total_items();
        $data = array_merge($this->settings, $data);

        $this->load->view('user/templates/header', $data);
        $this->load->view("user/$index", $data);
        $this->load->view('user/templates/footer', $data);
    }
}