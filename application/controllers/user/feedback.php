<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 16.02.15
 * Time: 17:32
 */

require_once 'my_controller.php';
class Feedback extends My_controller {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->helper('form');

        $this->form_validation->set_rules('name', 'Имя', 'trim|required|alpha_rus|min_length[2]|max_length[17]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('email', 'Email адрес', 'trim|required|min_length[4]|valid_email|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('message', 'Сообщение', 'trim|required|xxs_clean|prep_for_form');

        $data['about'] = 'Обратная связь';

        if ($this->form_validation->run() == TRUE) {

            $name = $this->input->post('name', TRUE);
            $email = $this->input->post('email', TRUE);
            $message = $this->input->post('message', TRUE);

            $this->base_model->feedback($name, $email, $message);
            $data['sussed'] = 'Отправлено!';
        }

        $this->set_title($data['about']);
        $this->template('feedback', $data);

    }
}

