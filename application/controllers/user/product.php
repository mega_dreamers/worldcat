<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 30.06.15
 * Time: 23:56
 */

require_once 'my_controller.php';
class Product extends My_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index($id = 0) {
        $data['goods'] = $this->u_goods_model->item_by_goods_id($id);
        $products = $data['goods'];
        $data['recommended'] = $this->base_model->recommended($products->categ_id);

        $meta = $this->u_goods_model->get_item($id);
        $data['meta_desc'] = $meta->meta_desc;
        $data['meta_key'] = $meta->meta_key;

        if(!empty($_POST['form_type'])) {
            if($_POST['form_type'] == 'comment') {$this->comment($id);}
        }
        $data['get_comment'] = $this->u_goods_model->commentaries($id);

        $this->set_title("$products->goods_title - Goods");
        $this->template('product', $data);
    }

    public function processing_form(){
        $id = $this->input->post('goods_id', true);
        $db = $this->u_goods_model->item_by_goods_id($id);
        $this->add_basket($id, $_POST['qty'], $db->price, $db->goods_title);
    }

    public function comment($id){
        $date = date("d F Y, H:i");

        $this->form_validation->set_rules('name', 'Имя', 'trim|required|alpha_rus|min_length[2]|max_length[17]|xss_clean|prep_for_form');
        $this->form_validation->set_rules('comment', 'Коментарий', 'trim|required|xss_clean|prep_for_form');

        $name = $this->input->post('name', true);
        $comment = $this->input->post('comment', true);

        if($this->form_validation->run() == true) {
            $this->base_model->recording_comment($name, $comment, $id, $date);
            redirect("product/$id");
        }
    }

    public function add_basket($goods, $qty, $price, $title) {
        $cart = array(
            'id' => $goods,
            'qty' => $qty,
            'price' => $price,
            'name' => md5($title),
        );
        $this->cart->insert($cart);
    }
}