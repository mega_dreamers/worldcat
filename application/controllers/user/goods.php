<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 09.03.15
 * Time: 19:33
 */


require_once 'my_controller.php';
class Goods extends My_controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('email');
        $this->load->helper('url');
        $this->load->model('a_categories_model');

        $email['newline'] = '\r\n';
        $email['wrapchars'] = '10000';
        $email['mailtype'] = 'html';
        $email['wordwrap'] = TRUE;
    }

    public function index($id = 0, $page = 1) {
        $this->load->library('pagination');
        $config = array(
            'base_url' => base_url().'goods/page/',
            'total_rows' =>  $this->u_goods_model->count(),
            'first_link' => 'В начало',
            'last_link' => 'В конец',
            'next_link' => '>',
            'prev_link' => '<',
            'num_links' => 20,
            'per_page' => 15,
        );
        $this->pagination->initialize($config);

        $data['categories'] = $this->a_categories_model->get();
        $data['about'] = 'Категории: ';


        if (!empty($this->input->get('search'))) {
            $request = $this->input->get('search', TRUE);
            $data['output'] = $this->mini_search($request);
        } else {
            if ($id != 0) {
                $data['list_goods'] = $this->u_goods_model->all_by_categ_id($id);
                $categ = $this->a_categories_model->get_item($id);
                $data['meta_desc'] = "Все кошки в категории '$categ->title'";
                $data['meta_key'] = "кошки, коты, $categ->title";
            } else {
                $data['list_goods'] = $this->u_goods_model->for_pagination($config['per_page'], $page);
                $data['meta_desc'] = "Продажа милых котят, не дорого";
                $data['meta_key'] = "кошки, коты, продажа кошек";
                $data['pagination'] = $this->pagination->create_links();
            }
        }


        $this->set_title('Goods');
        $this->template('goods', $data);
    }

    public function mini_search($request, $data = NULL) {
        if (!empty($request) AND strlen($request) < 3) {
            $data['form_error'] = "Запрос должен содержать не менее 3-х символов";
        } elseif(!empty($request)) {
            $data['list_goods'] = $this->u_goods_model->search($request);
            $data['request'] = "$request";
        }
        return $data;
    }

    public function search() {
            $data = NULL;
            $this->set_title('Поиск');
            $this->template('search', $data);
    }
}