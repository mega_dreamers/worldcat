<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 11.02.15
 * Time: 11:59
 */

require_once 'my_controller.php';
class Home extends My_controller {
    public function __construct() {
        parent::__construct();
    }

    public function index($link = '') {
        $data['about'] = 'И пусть весь мир скажет "Мяу!"';
        $meta = $this->base_model->get_content($link);
        $data['meta_desc'] = $meta->meta_desc;
        $data['meta_key'] = $meta->meta_key;

        $content = $meta;

        if (empty($content)) {
            show_404();
        }

        $data['get_image'] = $this->base_model->slide();
        $this->set_title($content->title);


        $data['content'] = $content;
        $this->template('home', $data);

    }

    public function promotions() {
        $data = array(
            'title' => 'Промо-акции',
            'about' => 'Друзья, хотите кусочек лета в феврале? Тогда, скорей принимайте участие в конкурсе и выигрывайте билет на посещение аквапарка в Терминале (г. Бровары).',
            'image' => 'http://proactions.com.ua/media/actions/2015/02/11/sunny.jpg.500x400_q95.jpg',
            'link' => 'https://www.facebook.com/sunny7UA/photos/a.170678316340044.42496.170571193017423/785341028207100/?type=1',
            'text' => "<b>Призы:</b> билет на посещение аквапарка<br /><br />
                        Условия участия в конкурсе:<br /><br />
                        <ul>
                            <li>Поставьте лайк странице Sunny7 в Facebook</li>
                            <li>Поделитесь с друзьями конкурсным постом (страничка должна быть открыта для просмотра)</li>
                            <li>В комментариях к посту опишите ваш пляжный отдых</li>
                            <li>В четверг, в 18-00 с помощью сервиса random.org будут определены двое победителей, которые получат по билету в аквапарк.</li>
                        </ul>"
        );

        $this->set_title($data['title']);
        $this->template('promotions', $data);
    }
}

/*<div id="container">
                <div id="slides">
                    <img src="http://www.naaf.no/upload/1.%20Allergi/katt-720.jpg" width="720" height="300" alt="Slide 1">

                    <img src="http://kotipes56.ru/upload/iblock/a97/cat1.jpg" width="720" height="300" alt="Slide 1">

                    <img src="../upload/siteimg.jpg" width="720" height="300" alt="Slide 3">
                </div>

                <a href="#" class="controls">Play</a>
                <p class="current_slide"></p>
            </div>*/