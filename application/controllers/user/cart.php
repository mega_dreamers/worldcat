<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 01.07.15
 * Time: 0:11
 */

require_once 'my_controller.php';
class Cart extends My_controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('email');
        $this->load->helper('url');

        $email['newline'] = '\r\n';
        $email['wrapchars'] = '10000';
        $email['mailtype'] = 'html';
        $email['wordwrap'] = TRUE;

        $this->email->initialize($email);

        $this->email->from($this->settings['email'], 'WorldCat.pe.hu');
    }

    public function index($action = '', $rowid = '') {
        $data['about'] = 'Корзина';

        foreach($this->cart->contents() as $item) {
            $data['data'][$item['id']] = $this->base_model->basket_name($item['id']);
        }

        switch ($action) {
            case 'delete':
                $this->cart->update(array('rowid' => $rowid, 'qty' => 0));
                redirect('/cart');
                break;

            case 'delete_all':
                $this->cart->destroy();
                redirect('/cart');
                break;

            case 'edit':
                $this->form_validation->set_rules('qty', 'Value', 'trim|required|numeric|integer|max_length[2]|xxs_clean|prep_for_form');
                if($this->form_validation->run() == TRUE) {
                    $this->cart->update(array('rowid' => $rowid, 'qty' => $_POST['qty']));
                }
                redirect('/cart');
                break;
        }

        $this->form_validation->set_rules('name', 'Имя', 'trim|required|alpha_rus|min_length[2]|max_length[17]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('telephone', 'Телефон', 'trim|required|min_length[4]|numeric|integer|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('email', 'Email адрес', 'trim|required|min_length[4]|xxs_clean|prep_for_form');

        $date = date("d F Y, H:i");

        $customer = array(
            'name' => $this->input->post('name', TRUE),
            'telephone' => $this->input->post('telephone', TRUE),
            'email' => $this->input->post('email', TRUE)
        );

        if($this->form_validation->run() == TRUE) {

             $array = array(
                'date' => $date,
                'name' => $customer['name'],
                'telephone' => $customer['telephone'],
                'email' => $customer['email'],
            );
            $this->base_model->add_customer($array);

            $insert_id = $this->base_model->id();

            foreach($this->cart->contents() as $item) {
                $array = array(
                    'order_id' => $insert_id,
                    'id_goods' => $item['id'],
                    'title_goods' => $data['data'][$item['id']]->goods_title,
                    'qty_goods' => $item['qty'],
                    'price_goods' => $item['price']*$item['qty']
                );
            $this->base_model->add_order($array);
            }

            $this->send_to_email($customer, $data);
        }

        $this->set_title('Cart');
        $this->template('basket', $data);
    }

    public function show_total_items() {
        echo '<img src="/img/cart.png" style="width: 35px">'.$this->cart->total_items();
    }

    protected function send_to_email($customer, $data) {
        # Send to User

        $this->email->to($customer['email']);
        $this->email->subject('Ваш заказ принят!');
        $message = 'Здравствуйте '. $customer['name'] .'!<br />'.
            ' <b>На это сообщение отвечать не нужно</b>. <br />Вы его получили, потому что сделали заказ на <a href="http://worldcat.pe.hu/">нашем сайте</a>. <br />'.
            ' Ваш заказ состоит из: <br /><ol>';
        foreach($this->cart->contents() as $o) {
            $message .= '<li>"'.$data['data'][$o['id']]->goods_title.'" '.$o['qty'].'шт., за $'.$o['price']*$o['qty'].'</li>';
        }
        $message .= '</ol> Сумма вашего заказа составляет <b>$'.$this->cart->total().'</b>.';
        $message .= '<br /><br /> Наш менеджер свяжется с вами в ближайшее время.';

        $this->email->message($message);
        $this->email->send();

        #Send to Admin

        $this->email->to('nik4ik98@gmail.com');
        $this->email->subject('Новый заказ на сайте');
        $this->email->message('Клиент под именем <b>'.$customer['name'].'</b> заказал товары на сумму <b>$'.$this->cart->total().'</b>. <br /> Его email - '.$customer['name'].', и номер телефона - "'.$_POST['telephone'].
            '". <br /> Чтобы посмотреть его заказ, перейдите по <a href="http://worldcat.pe.hu/admin/orders">этой</a> ссылке.');
        $this->email->send();

        $this->cart->destroy();
        redirect('/cart/order_completed');
    }

    public function order_completed() {
        $data['completed'] = 'Заказ оформлен.<br /> <div style="margin-top: 35px">Спасибо за покупку!</div>';

        $this->set_title('Заказ оформлен!');
        $this->template('order_completed', $data);
    }
}