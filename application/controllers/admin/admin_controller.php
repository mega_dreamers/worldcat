<?php

/**
 * Class Admin_controller
 * @property base_model base_model
 * @property a_accounts_model a_accounts_model
 * @property a_auth_model a_auth_model
 * @property a_messages_model a_messages_model
 * @property a_pages_model a_pages_model
 * @property a_categories_model a_categories_model
 * @property a_goods_model a_goods_model
 * @property a_orders_model a_orders_model
 * @property a_site_config_model a_site_config_model
 * @property CI_Loader load
 * @property CI_session session
 * @property CI_Form_validation form_validation
 */

class Admin_controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'base_model',
            'a_accounts_model',
            'a_auth_model',
            'a_messages_model',
            'a_pages_model',
            'a_categories_model',
            'a_goods_model',
            'a_orders_model',
            'a_site_config_model'
        ));

        $this->load->library('session');
        $this->load->helper('url');

        $this->settings = array(
            'page_name' => 'ADMIN - Сайт о кошках',
            'page_title' => '',
        );
    }

    protected function set_title($string) {
        $this->settings['page_title'] = $string;
    }

    protected function template($index, $data) {
        $data['get_admin_menu'] = $this->base_model->get_menu();
        $data['username'] = $this->session->userdata('username');
        $data['session_id'] = $this->session->userdata('id_user');
        $data['count_mess'] = $this->db->where('status', 'new')->count_all_results('feedback');
        $data = array_merge($this->settings, $data);

        $this->load->view('admin/templates/header', $data);
        $this->load->view($index, $data);
        $this->load->view('admin/templates/footer', $data);
    }
}