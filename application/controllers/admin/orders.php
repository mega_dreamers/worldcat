<?php

require_once 'admin_controller.php';
class Orders extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $session_id = $this->session->userdata('id_user');
        if(empty($session_id)) {redirect('/admin');}
    }

    public function index() {
        $data['username'] = $this->session->userdata('username');
        $data['customer'] = $this->a_orders_model->get();

        $this->set_title('orders');
        $this->template('admin/orders', $data);
    }

    public function order($id) {
        $data['username'] = $this->session->userdata('username');
        $data['customer'] = $this->a_orders_model->item_customer($id);
        $data['order'] = $this->a_orders_model->item_orders($id);

        $this->form_validation->set_rules('status', 'Order status', 'trim|required|xxs_clean|prep_for_form');

        if($this->form_validation->run() == TRUE) {
            $status = $this->input->post('status', TRUE);
            $this->a_orders_model->update_status($id, $status);
            redirect("admin/order/$id");
        }

        $this->set_title('order');
        $this->template('admin/order', $data);
    }

    public function delete($id) {
        $this->a_orders_model->delete($id);
        redirect('/admin/orders');
    }
}
