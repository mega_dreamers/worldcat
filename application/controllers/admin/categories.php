<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 11.03.15
 * Time: 21:33
 */

require_once 'admin_controller.php';
class Categories extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $session_id = $this->session->userdata('id_user');
        if(empty($session_id)) {redirect('/admin');}
    }

    public function index() {
        $data['username'] = $this->session->userdata('username');
        $data['categories'] = $this->a_categories_model->get();

        $this->form_validation->set_rules('title', 'Categories title', 'trim|required|max_length[20]|xxs_clean|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $title = $this->input->post('title', TRUE);
            $this->a_categories_model->add($title);
            redirect('/admin/categories');
        }

        $this->set_title('categories');
        $this->template('admin/categories', $data);
    }

    public function categories_edit($id) {
        $data['username'] = $this->session->userdata('username');
        $data['content'] = $this->a_categories_model->get_item($id);

        $this->form_validation->set_rules('id', 'Categories id', 'trim|required|integer|numeric|max_length[20]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('title', 'Categories title', 'trim|required|max_length[20]|xxs_clean|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $array = array(
                'categ_id' => $this->input->post('id', TRUE),
                'title' =>$this->input->post('title', TRUE),
            );
            $this->a_categories_model->edit($id, $array);
            redirect('/admin/categories');
        }

        $this->set_title('edit');
        $this->template('admin/categories_edit', $data);
    }

    public function delete($id) {
        $this->a_categories_model->delete($id);
        redirect('/admin/categories');
    }
}
